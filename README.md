Instructions for applicants:  

 1. Unzip the attached file  
 
 2. The manual test case creation section can be found in file "LimelightHealthQA_Exam" and instructions are given  
 
 3. The automation section is in the folder "limelightqatestAutomation". This should be imported into your chosen IDE and worked on.   
 
 4. Download the Selenium drivers you require via http://www.seleniumhq.org/projects/webdriver/ or use the firefox driver provided via the Selenium library. To use the driver place it in the driverEXE folder and update the reference to the driver in the driver factory class.  
 
 5. The automation exam questions can be found in the README file found at the top of the folder. We encourage applicants to use a POM approach when answering the questions and suggest using http://toolsqa.com/selenium-webdriver/page-object-model/ as a basic guide  
 
 6. Once finished with the exam put both your automation folder and your manual test case answers in a folder and zip up the folder. (Note the driver executable should be removed before zipping up the folders)  

If you have any questions or any problems with the exam please contact Raymond Kelly at: raymond@limelighthealth.com